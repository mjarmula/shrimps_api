Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'user_token' => 'user_token#create'
      get 'authenticate' => 'auth#authenticate'

      resources :rpis, only: %i(index) do
        resources :rpi_devices, only: %i(index)
      end
    end
  end
end
