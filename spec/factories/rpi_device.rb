FactoryGirl.define do
  factory(:rpi_device) do
    config_group_id 1
    icon_off nil
    icon_on nil
    name "heater"
    pin_btn "0"
    pin_pwr "0"
    rpi_id 1
  end
end