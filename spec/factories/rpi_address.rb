FactoryGirl.define do
  factory(:rpi_address) do
    city "Kiehntown"
    country "Bhutan"
    post_code "98337"
    rpi_id 1
    street "ToFactory: RubyParser exception parsing this attribute after factory generation"
  end
end