FactoryGirl.define do
  factory(:adm_address) do
    city "Port Frederic"
    country "Christmas Island"
    post_code "49982-4836"
    rpi_user_id 1
    street "ToFactory: RubyParser exception parsing this attribute after factory generation"
  end
end