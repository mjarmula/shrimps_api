FactoryGirl.define do
  factory(:data_heater) do
    config_trigger_id 1
    device_status false
    rpi_device_id 1
  end
end