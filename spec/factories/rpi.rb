FactoryGirl.define do
  factory(:rpi) do
    hostname "marktark.org"
    icon nil
    joined "2018-06-02T01:33Z"
    rpi_user_id 1
  end
end