FactoryGirl.define do
  factory(:user) do
    email "admin@example.com"
    mobile "ToFactory: RubyParser exception parsing this attribute after factory generation"
    name "Clarissa"
    password_digest "ToFactory: RubyParser exception parsing this attribute after factory generation"
    role "admin"
    status true
    surname "Mann"
    username "admin"
  end
end