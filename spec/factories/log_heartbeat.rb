FactoryGirl.define do
  factory(:log_heartbeat) do
    device_status false
    rpi_device_id 1
  end
end