require 'rails_helper'

RSpec.describe OwnerScope do
  subject { described_class.new(user, scope) }
  let(:user) { create(:user) }
  let(:rpi) { create(:rpi, user: user) }
  let(:scope) { Rpi }

  context 'when user is owner' do
    describe '#resolve' do
      it 'returns list of records' do
        expect(subject.resolve).to eq([rpi])
      end
    end
  end

  context 'when user is not owner' do
    let(:other_user) { create(:user) }
    let(:rpi) { create(:rpi, user: other_user) }

    describe '#resolve' do
      it 'retuns empty array' do
        expect(subject.resolve).to eq([])
      end
    end
  end
end
