require 'rails_helper'

RSpec.describe RpiDevices::DataReadingsService do
  describe '.device' do
    let(:user) { create(:user) }
    let(:rpi) { create(:rpi, user: user) }
    let(:config_group) { create(:config_group) }
    let(:device) { create(:rpi_device, rpi: rpi, config_group: config_group) }
    let(:temperature) { create(:data_temperature, rpi_device: device) }

    describe '.data_reading' do
      subject { described_class.data_reading(device) }

      context 'when sensor belongs to group temperatures' do
        it 'returns temperature sensor data' do
          is_expected.to eq(device.data_temperatures)
        end
      end
    end
  end
end