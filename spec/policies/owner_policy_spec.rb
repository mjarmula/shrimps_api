require 'rails_helper'

RSpec.describe OwnerPolicy do
  subject { described_class.new(user, record) }

  let(:user) { create(:user) }
  let(:record) { create(:rpi, user: user) }

  context 'when user is not owner' do
    describe '#update?' do
      it 'returns true' do
        expect(subject.update?).to be_truthy
      end
    end

    describe '#show?' do
      it 'returns true' do
        expect(subject.show?).to be_truthy
      end
    end
  end

  context 'when user is not owner' do
    let(:different_user) { create(:user) }
    before do
      record.user = different_user
    end

    describe '#update?' do
      it 'returns false' do
        expect(subject.update?).to be_falsy
      end
    end

    describe '#show?' do
      it 'returns false' do
        expect(subject.show?).to be_falsy
      end
    end
  end

  context 'when record doesnt exist' do
    subject { described_class.new(user, record) }
    let(:scope) { double('Scope') }

    before do
      allow_any_instance_of(OwnerPolicy).to receive(:scope).and_return(scope)
      allow(scope).to receive_message_chain(:where, :exists?).and_return(false)
    end

    it 'returns false' do
      expect(subject.show?).to be_falsy
    end
  end
end
