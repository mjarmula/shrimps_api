require 'rails_helper'

RSpec.describe Rpi do
  it { should belong_to(:user) }

  it { should have_many(:rpi_addresses) }
  it { should have_many(:rpi_devices) }
  it { should have_many(:config_groups) }
end
