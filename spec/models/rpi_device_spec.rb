require 'rails_helper'

RSpec.describe RpiDevice do
  it { should belong_to(:rpi) }
  it { should belong_to(:config_group) }

  it { should have_many(:data_temperatures) }
  it { should have_many(:data_lights) }
  it { should have_many(:data_heaters) }
end
