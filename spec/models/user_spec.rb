require 'rails_helper'

RSpec.describe User do
  it { should have_many(:adm_addresses) }
  it { should have_many(:rpis) }
end
