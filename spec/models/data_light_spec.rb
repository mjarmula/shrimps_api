require 'rails_helper'

RSpec.describe DataLight do
  it { should belong_to(:rpi_device) }
  it { should belong_to(:config_trigger) }
end
