# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180524231252) do

  create_table "adm_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "city", null: false
    t.string "country", null: false
    t.string "street", null: false
    t.string "post_code", null: false
    t.bigint "rpi_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rpi_user_id"], name: "index_adm_addresses_on_rpi_user_id"
  end

  create_table "config_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_config_groups_on_name", unique: true
  end

  create_table "config_triggers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_config_triggers_on_name", unique: true
  end

  create_table "data_heaters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "rpi_device_id"
    t.bigint "config_trigger_id"
    t.boolean "device_status", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["config_trigger_id"], name: "index_data_heaters_on_config_trigger_id"
    t.index ["rpi_device_id"], name: "index_data_heaters_on_rpi_device_id"
  end

  create_table "data_lights", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "rpi_device_id"
    t.bigint "config_trigger_id"
    t.boolean "device_status", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["config_trigger_id"], name: "index_data_lights_on_config_trigger_id"
    t.index ["rpi_device_id"], name: "index_data_lights_on_rpi_device_id"
  end

  create_table "data_temperatures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "rpi_device_id"
    t.string "sensor_data", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rpi_device_id"], name: "index_data_temperatures_on_rpi_device_id"
  end

  create_table "log_heartbeats", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "rpi_device_id"
    t.boolean "device_status", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rpi_device_id"], name: "index_log_heartbeats_on_rpi_device_id"
  end

  create_table "rpi_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "city", null: false
    t.string "country", null: false
    t.string "street", null: false
    t.string "post_code", null: false
    t.bigint "rpi_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rpi_id"], name: "index_rpi_addresses_on_rpi_id"
  end

  create_table "rpi_devices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "rpi_id"
    t.bigint "config_group_id"
    t.string "name", null: false
    t.string "icon_on"
    t.string "icon_off"
    t.string "pin_pwr", default: "0", null: false
    t.string "pin_btn", default: "0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["config_group_id"], name: "index_rpi_devices_on_config_group_id"
    t.index ["rpi_id"], name: "index_rpi_devices_on_rpi_id"
  end

  create_table "rpi_networks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "rpi_id"
    t.string "lan_ip"
    t.string "wlan_ip"
    t.string "wan_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rpi_id"], name: "index_rpi_networks_on_rpi_id"
  end

  create_table "rpi_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.string "surname", null: false
    t.string "username", null: false
    t.string "role", default: "user", null: false
    t.string "email", null: false
    t.string "mobile", null: false
    t.string "password_digest", null: false
    t.boolean "status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rpis", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "hostname", null: false
    t.bigint "rpi_user_id"
    t.string "icon"
    t.datetime "joined", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hostname"], name: "index_rpis_on_hostname", unique: true
    t.index ["rpi_user_id"], name: "index_rpis_on_rpi_user_id"
  end

  add_foreign_key "adm_addresses", "rpi_users"
  add_foreign_key "data_heaters", "config_triggers"
  add_foreign_key "data_heaters", "rpi_devices"
  add_foreign_key "data_lights", "config_triggers"
  add_foreign_key "data_lights", "rpi_devices"
  add_foreign_key "data_temperatures", "rpi_devices"
  add_foreign_key "log_heartbeats", "rpi_devices"
  add_foreign_key "rpi_addresses", "rpis"
  add_foreign_key "rpi_devices", "config_groups"
  add_foreign_key "rpi_devices", "rpis"
  add_foreign_key "rpi_networks", "rpis"
  add_foreign_key "rpis", "rpi_users"
end
