class CreateRpiUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :rpi_users do |t|
      t.string :name, null: false
      t.string :surname, null: false
      t.string :username, null: false
      t.string :role, null: false, default: 'user'
      t.string :email, null: false
      t.string :mobile, null: false
      t.string :password_digest, null: false
      t.boolean :status, default: true

      t.timestamps
    end
  end
end
