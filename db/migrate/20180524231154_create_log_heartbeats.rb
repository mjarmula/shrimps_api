class CreateLogHeartbeats < ActiveRecord::Migration[5.1]
  def change
    create_table :log_heartbeats do |t|
      t.references :rpi_device, foreign_key: true
      t.boolean :device_status, null: false

      t.timestamps
    end
  end
end
