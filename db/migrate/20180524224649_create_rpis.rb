class CreateRpis < ActiveRecord::Migration[5.1]
  def change
    create_table :rpis do |t|
      t.string :hostname, null: false, index: { unique: true }
      t.references :rpi_user, foreign_key: true
      t.string :icon, default: nil
      t.datetime :joined, null: false, default: -> { 'CURRENT_TIMESTAMP' }

      t.timestamps
    end
  end
end
