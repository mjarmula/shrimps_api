class CreateAdmAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :adm_addresses do |t|
      t.string :city, null: false
      t.string :country, null: false
      t.string :street, null: false
      t.string :post_code, null: false
      t.references :rpi_user, foreign_key: true

      t.timestamps
    end
  end
end
