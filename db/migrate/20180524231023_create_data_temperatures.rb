class CreateDataTemperatures < ActiveRecord::Migration[5.1]
  def change
    create_table :data_temperatures do |t|
      t.references :rpi_device, foreign_key: true
      t.string :sensor_data, null: false

      t.timestamps
    end
  end
end
