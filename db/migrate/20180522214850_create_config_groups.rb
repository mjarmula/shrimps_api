class CreateConfigGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :config_groups do |t|
      t.string :name, null: false, index: { unique: true }
      t.string :icon, default: nil

      t.timestamps
    end
  end
end
