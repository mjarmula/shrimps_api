class CreateConfigTriggers < ActiveRecord::Migration[5.1]
  def change
    create_table :config_triggers do |t|
      t.string :name, null: false, index: { unique: true }
      t.string :icon, default: nil

      t.timestamps
    end
  end
end
