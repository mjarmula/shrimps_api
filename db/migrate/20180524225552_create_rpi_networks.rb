class CreateRpiNetworks < ActiveRecord::Migration[5.1]
  def change
    create_table :rpi_networks do |t|
      t.references :rpi, foreign_key: true
      t.string :lan_ip, default: nil
      t.string :wlan_ip, default: nil
      t.string :wan_ip, default: nil

      t.timestamps
    end
  end
end
