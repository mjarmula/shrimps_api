class CreateDataHeaters < ActiveRecord::Migration[5.1]
  def change
    create_table :data_heaters do |t|
      t.references :rpi_device, foreign_key: true
      t.references :config_trigger, foreign_key: true
      t.boolean :device_status, null: false

      t.timestamps
    end
  end
end
