class CreateRpiDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :rpi_devices do |t|
      t.references :rpi, foreign_key: true
      t.references :config_group, foreign_key: true
      t.string :name, null: false
      t.string :icon_on, default: nil
      t.string :icon_off, default: nil
      t.string :pin_pwr, null: false, default: '0'
      t.string :pin_btn, null: false, default: '0'

      t.timestamps
    end
  end
end
