Rpi.all.each do |rpi|
  RpiNetwork.create(
    wlan_ip: Faker::Internet.ip_v4_address,
    wan_ip: Faker::Internet.ip_v4_address,
    rpi: rpi
  )
end

puts 'Created rpi networks'
