RpiDevice.all.each do |device|
  LogHeartbeat.create(
    rpi_device: device,
    device_status: Random.rand(0..1)
  )
end

puts 'Created log heartbeats'
