Rpi.all.each do |rpi|
  RpiAddress.create(
    city: Faker::Address.city,
    country: Faker::Address.country,
    street: Faker::Address.street_address,
    post_code: Faker::Address.postcode,
    rpi: rpi
  )
end

puts 'Created addresses for rpis'
