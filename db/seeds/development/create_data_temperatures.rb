RpiDevice.where(name: 'heater').each do |rpi_device|
  10.times do
    DataTemperature.create(
      rpi_device: rpi_device,
      sensor_data: Random.rand(12..30)
    )
  end
end

puts 'Created data temperatures for each device'
