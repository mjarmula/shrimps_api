User.all.each do |user|
  AdmAddress.create(
    city: Faker::Address.city,
    country: Faker::Address.country,
    street: Faker::Address.street_address,
    post_code: Faker::Address.postcode,
    user: user
  )
end

puts 'Created address for each user'
