RpiDevice.where(name: 'lamp').each do |rpi_device|
  10.times do
    DataLight.create(
      rpi_device: rpi_device,
      device_status: Random.rand(0..1),
      config_trigger_id: 1
    )
  end
end

puts 'Created data lights'
