User.create(
  name: Faker::Name.first_name,
  surname: Faker::Name.last_name,
  username: 'admin',
  email: 'admin@example.com',
  role: 'admin',
  mobile: Faker::PhoneNumber.cell_phone,
  password: 'password'
)
User.create(
  name: Faker::Name.first_name,
  surname: Faker::Name.last_name,
  username: 'user',
  email: 'admin@example.com',
  role: 'user',
  mobile: Faker::PhoneNumber.cell_phone,
  password: 'password'
)

puts 'Created 2 users, one admin and one standard user'
