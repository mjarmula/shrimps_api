Rpi.all.each do |rpi|
  RpiDevice.create(
    rpi: rpi,
    name: 'heater',
    config_group_id: 1
  )
end

puts 'Created one heater device for each rpi'

Rpi.all.each do |rpi|
  RpiDevice.create(
    rpi: rpi,
    name: 'lamp',
    config_group_id: 2
  )
end

puts 'Created one lamp device for each rpi'
