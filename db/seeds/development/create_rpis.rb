User.all.each do |user|
  Rpi.create(
    user: user,
    hostname: Faker::Internet.domain_name
  )
end

puts 'Created rpis for each user'
