class OwnerScope < ApplicationScope
  def resolve
    super.where(user: user)
  end
end
