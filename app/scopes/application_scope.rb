class ApplicationScope
  attr_reader :user, :scope
  private :user, :scope

  def initialize(user, scope)
    @user  = user
    @scope = scope
  end

  def self.resolve_for(user:, scope:)
    new(user, scope).resolve
  end

  def resolve
    scope
  end
end
