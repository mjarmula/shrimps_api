class DataTemperatureSerializer < ActiveModel::Serializer
  attributes :sensor_data

  belongs_to :rpi_device
end
