class RpiDeviceSerializer < ActiveModel::Serializer
  attributes :name, :id, :last_reading

  belongs_to :config_group

  def last_reading
    RpiDevices::DataReadingsService.data_reading(device).last.read_value
  end

  private

  def device
    object.object
  end
end
