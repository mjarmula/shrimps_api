class ConfigGroupSerializer < ActiveModel::Serializer
  attributes :name

  has_many :rpi_devices
end
