module RpiDevices
  class DataReadingsService
    attr_reader :device, :group_key
    private :device, :group_key

    def initialize(device)
      @device = device
      @group_key = device.config_group.name.to_sym
    end

    def self.data_reading(device)
      new(device).data_reading
    end

    def data_reading
      device.public_send(eligible_association)
    end

    def eligible_association
      data_reading_associations.fetch(group_key)
    end

    private

    def data_reading_associations
      {
        temperature: :data_temperatures,
        light: :data_lights,
        heater: :data_heaters
      }
    end
  end
end
