class OwnerPolicy < ApplicationPolicy
  def update?
    super || record_owner?
  end

  def show?
    super && record_owner?
  end

  private

  def record_owner?
    record.user == user
  end
end
