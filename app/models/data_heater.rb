class DataHeater < ApplicationRecord
  belongs_to :rpi_device
  belongs_to :config_trigger

  def read_value
    device_status
  end
end
