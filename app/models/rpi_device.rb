class RpiDevice < ApplicationRecord
  belongs_to :rpi
  belongs_to :config_group

  has_many :data_temperatures
  has_many :data_lights
  has_many :data_heaters
end
