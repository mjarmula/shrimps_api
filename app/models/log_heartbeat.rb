class LogHeartbeat < ApplicationRecord
  belongs_to :rpi_device

  def read_value
    device_status
  end
end
