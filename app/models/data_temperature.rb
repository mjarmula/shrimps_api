class DataTemperature < ApplicationRecord
  belongs_to :rpi_device

  def read_value
    sensor_data
  end
end
