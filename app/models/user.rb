class User < ApplicationRecord
  has_secure_password

  has_many :adm_addresses, foreign_key: 'rpi_user_id'
  has_many :rpis, foreign_key: 'rpi_user_id'

  self.table_name = 'rpi_users'

  def admin?
    role == 'admin'
  end
end
