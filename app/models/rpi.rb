class Rpi < ApplicationRecord
  belongs_to :user, foreign_key: 'rpi_user_id'

  has_many :rpi_addresses
  has_many :rpi_devices
  has_many :config_groups, through: :rpi_devices
end
