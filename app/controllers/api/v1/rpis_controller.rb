module Api
  module V1
    class RpisController < SecuredController
      def index
        rpis = RpiPolicy::Scope.resolve_for(user: current_user, scope: Rpi)

        user_rpi = rpis.map { |rpi| RpiSerializer.new(rpi) }
        render json: user_rpi
      end
    end
  end
end
