module Api
  module V1
    class RpiDevicesController < SecuredController
      def index
        rpi = Rpi.joins(rpi_devices: :config_group).find(params[:rpi_id])
        authorize(rpi, :show?)

        rpi_devices = rpi.rpi_devices.map do |device|
          RpiDeviceSerializer.new(device)
        end

        render json: rpi_devices
      end
    end
  end
end
